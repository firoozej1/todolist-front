import { TodoStatus } from "./TodoStatus";

type Todo = {
    id: string;
    title: string;
    status: TodoStatus;
};

export type { Todo };
