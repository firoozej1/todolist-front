import React from "react";
import { Provider } from "react-redux";
import { SnackbarProvider } from "notistack";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import store from "store";
import { styled } from "@mui/material/styles";
import ToDoList from "./todo/todoList";
import Message from "./shared/message";

const StyledContainer = styled(Container)(() => ({
    marginTop: "50px",
}));

const App: React.FC = () => {
    return (
        <Provider store={store}>
            <SnackbarProvider maxSnack={1}>
                <CssBaseline />
                <Message />
                <StyledContainer maxWidth="md">
                    <ToDoList />
                </StyledContainer>
            </SnackbarProvider>
        </Provider>
    );
};

export default App;
