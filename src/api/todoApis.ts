import { Todo } from "types";
import { get, patch, post, remove } from "./base";
const TodoApis = {
    getTodoTitle: async ({ customError }: any = {}) => {
        const url = `/getTodoName`;
        return await get({ url, customError });
    },
    getTodos: async ({ customError }: any = {}) => {
        const url = `/getTodos`;
        return await get({ url, customError });
    },
    addTodo: async (Todo: Todo, { customError }: any = {}) => {
        const url = `/addTodo`;
        return await post({ url, payload: Todo, customError });
    },
    removeTodos: async (titles: string[], { customError }: any = {}) => {
        const url = `/removeTodos`;
        return await remove({ url, payload: titles, customError });
    },
    toggleTodosStatus: async (ids: string[], { customError }: any = {}) => {
        const url = `/toggleTodosStatus`;
        return await patch({ url, payload: ids, customError });
    },
};

export default TodoApis;
