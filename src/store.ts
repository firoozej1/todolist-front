import { configureStore } from "@reduxjs/toolkit";
import { messageReducer, todosReducer } from "reducer";

const store = configureStore({
    reducer: {
        message: messageReducer,
        todos: todosReducer,
    },
});

export type AppDispatch = typeof store.dispatch;
export type AppState = ReturnType<typeof store.getState>;

export default store;
