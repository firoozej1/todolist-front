import React from "react";
import TableCell from "@mui/material/TableCell";
import TableRow from "@mui/material/TableRow";
import Checkbox from "@mui/material/Checkbox";
import { styled } from "@mui/material/styles";
import { selectTodoById } from "reducer";
import { useSelector } from "react-redux";
import { EntityId } from "@reduxjs/toolkit";
import { AppState } from "store";
import { Todo, TodoStatus } from "types";

type PropsType = {
    id: EntityId;
    selected: boolean;
    onSelectClick: (event: React.ChangeEvent<HTMLInputElement>, id: string) => void;
};

const ToDoListItem: React.FC<PropsType> = ({ id, selected, onSelectClick }) => {
    const todo: Todo | undefined = useSelector((state: AppState) => selectTodoById(state, id));
    if (!todo) return null;
    return (
        <StyledTableRow>
            <TableCell padding="checkbox">
                <Checkbox
                    color="primary"
                    checked={selected}
                    onChange={e => onSelectClick(e, todo.id)}
                />
            </TableCell>
            <TableCell>{todo.title}</TableCell>
            <TableCell width={200}>
                {todo.status === TodoStatus.DONE ? "Done" : "Not Done"}
            </TableCell>
        </StyledTableRow>
    );
};

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    "&:nth-of-type(odd)": {
        backgroundColor: theme.palette.action.hover,
    },
}));

export default ToDoListItem;
