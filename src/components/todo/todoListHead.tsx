import React, { useState } from "react";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import Search from "./search";
import { CustomButton } from "components/shared";
import AddToDoDialog from "./addToDoDialog";

const TodoListHead: React.FC = () => {
    const [addToDoDialog, setAddToDoDialog] = useState({
        open: false,
    });
    return (
        <>
            <Paper sx={{ padding: "10px", marginBottom: "10px" }}>
                <Grid container alignItems="center">
                    <Grid item md={8} sm={12} xs={12}>
                        <CustomButton
                            size="medium"
                            onClick={() => setAddToDoDialog({ open: true })}>
                            Add Todo
                        </CustomButton>
                    </Grid>
                    <Grid item md={4} sm={12} xs={12}>
                        <Search />
                    </Grid>
                </Grid>
            </Paper>

            <AddToDoDialog
                open={addToDoDialog.open}
                onClose={() => setAddToDoDialog({ open: false })}
            />
        </>
    );
};

export default React.memo(TodoListHead);
