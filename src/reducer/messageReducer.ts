import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    type: "",
    text: "",
};

const messageSlice = createSlice({
    name: "message",
    initialState,
    reducers: {
        apiErrorOccured(state, action) {
            return {
                type: "error",
                text: action.payload,
            };
        },
    },
});

export const { apiErrorOccured } = messageSlice.actions;

const messageReducer = messageSlice.reducer;

export { messageReducer };
