import React, { useState } from "react";
import TextField from "@mui/material/TextField";
import { useDispatch } from "react-redux";
import { titleFilterChanged } from "reducer";

const Search: React.FC = () => {
    const [title, setTitle] = useState("");
    const dispatch = useDispatch();

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setTitle(event.target.value);
        dispatch(titleFilterChanged(event.target.value));
    };

    return (
        <TextField
            label="Search"
            size="small"
            variant="standard"
            value={title}
            onChange={handleChange}
        />
    );
};

export default Search;
