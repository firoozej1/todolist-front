import React, { ReactNode } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import CircularProgress from "@mui/material/CircularProgress";
import { styled } from "@mui/material/styles";

type PropTypes = {
    title: ReactNode;
    open: boolean;
    onClose: Function;
    onOk: Function;
    okButtonProps?: any;
    confirmLoading?: boolean;
    children: ReactNode;
};

const CustomDialog: React.FC<PropTypes> = ({
    open,
    onClose,
    onOk,
    title,
    okButtonProps,
    confirmLoading = false,
    children,
    ...rest
}) => {
    const handleEnter = (e: React.KeyboardEvent<HTMLDivElement>) => {
        if (e.key === "Enter") {
            onOk();
        }
    };
    return (
        <Dialog
            open={open}
            onClose={() => onClose()}
            onKeyUp={handleEnter}
            maxWidth={"sm"}
            fullWidth={true}
            {...rest}>
            <StyledDialogTitle>{title}</StyledDialogTitle>
            <StyledDialogContent>{children}</StyledDialogContent>
            <DialogActions>
                <Button onClick={() => onClose()}>Cancel</Button>
                <Button
                    onClick={() => onOk()}
                    startIcon={confirmLoading ? <CircularProgress size="1rem" /> : undefined}
                    size="small"
                    variant="contained"
                    disabled={confirmLoading}
                    {...okButtonProps}>
                    OK
                </Button>
            </DialogActions>
        </Dialog>
    );
};

const StyledDialogTitle = styled(DialogTitle)(({ theme }) => ({
    border: `1px solid ${theme.palette.divider}`,
    borderWidth: "0 0 1px 0",
}));

const StyledDialogContent = styled(DialogContent)(() => ({
    padding: "24px !important",
}));

export { CustomDialog };
