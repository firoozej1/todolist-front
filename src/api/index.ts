import TodoApis from "./todoApis";

const Provider = {
    todoApi: { ...TodoApis },
};

export default Provider;
