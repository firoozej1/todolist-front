import axios from "axios";
import store from "store";

const domain = "http://localhost:4000";

const config = {
    timeout: 1000 * 5,
    headers: {
        "Content-Type": "application/json",
    },
};

const request = async ({
    url,
    method,
    payload = {},
    customError = "",
    customConfig = null,
}: any) => {
    try {
        const { data } = await axios.request({
            method,
            url: url.indexOf("http") === -1 ? `${domain}${url}` : url,
            data: payload,
            ...(customConfig ? { ...config, ...customConfig } : config),
        });
        return data;
    } catch (error: any) {
        const { apiErrorOccured } = await import("reducer/messageReducer");
        store.dispatch(apiErrorOccured(customError ? customError : error.message));

        return Promise.reject(error);
    }
};

const get = async ({ url, customError, customConfig }: any) => {
    return await request({
        url,
        method: "get",
        customError,
        customConfig,
    });
};

const post = async ({ url, payload, customError, customConfig }: any) => {
    return await request({
        url,
        method: "post",
        payload,
        customError,
        customConfig,
    });
};

const patch = async ({ url, payload, customError, customConfig }: any) => {
    return await request({
        url,
        method: "patch",
        payload,
        customError,
        customConfig,
    });
};

const remove = async ({ url, payload, customError, customConfig }: any) => {
    return await request({ url, method: "delete", payload, customConfig, customError });
};

export { get, post, patch, remove };
