import * as React from "react";
import { styled } from "@mui/material/styles";
import Button from "@mui/material/Button";
import CircularProgress from "@mui/material/CircularProgress";

const StyledCustomButton = styled(Button)({
    textTransform: "none",
});

type PropTypes = {
    children: React.ReactNode;
    loading?: boolean;
    [key: string]: any;
};

const CustomButton: React.FC<PropTypes> = ({ children, loading, ...rest }) => {
    return (
        <StyledCustomButton
            variant="contained"
            size="small"
            disabled={loading}
            startIcon={loading ? <CircularProgress size="1rem" /> : undefined}
            {...rest}>
            {children}
        </StyledCustomButton>
    );
};

export { CustomButton };
