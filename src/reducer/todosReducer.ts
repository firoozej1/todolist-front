import {
    createAsyncThunk,
    createEntityAdapter,
    createSelector,
    createSlice,
} from "@reduxjs/toolkit";
import Provider from "api";
import { AppState } from "store";
import { Todo, TodoStatus } from "types";

const todosAdapter = createEntityAdapter<Todo>();

const initialState = todosAdapter.getInitialState({
    loading: false,
    addLoading: false,
    deleteLoading: false,
    changeStatusLoading: false,
    filter: {
        title: "",
    },
});

const todosSlice = createSlice({
    name: "todos",
    initialState,
    reducers: {
        titleFilterChanged(state, action) {
            state.filter.title = action.payload;
        },
    },
    extraReducers: builder => {
        builder.addCase(getTodos.pending, state => {
            state.loading = true;
        });
        builder.addCase(getTodos.fulfilled, (state, action) => {
            state.loading = false;
            todosAdapter.setAll(state, action.payload);
        });
        builder.addCase(addTodo.pending, state => {
            state.addLoading = true;
        });
        builder.addCase(addTodo.fulfilled, (state, action) => {
            state.addLoading = false;
            todosAdapter.addOne(state, action.payload);
        });
        builder.addCase(deleteToDo.pending, state => {
            state.deleteLoading = true;
        });
        builder.addCase(deleteToDo.fulfilled, (state, action) => {
            state.deleteLoading = false;
            todosAdapter.removeMany(state, action.payload);
        });
        builder.addCase(toggleTodoStatus.pending, state => {
            state.changeStatusLoading = true;
        });
        builder.addCase(toggleTodoStatus.fulfilled, (state, action) => {
            state.changeStatusLoading = false;
            todosAdapter.updateMany(
                state,
                action.payload.map((todo: Todo) => ({ id: todo.id, changes: todo }))
            );
        });
        builder.addCase(toggleTodoStatus.rejected, (state, action) => {
            state.changeStatusLoading = false;
            try {
                todosAdapter.updateMany(
                    state,
                    action.meta.arg.map((id: string) => ({
                        id,
                        changes: {
                            status:
                                (state.entities[id] as Todo).status === TodoStatus.DONE
                                    ? TodoStatus.UNDONE
                                    : TodoStatus.DONE,
                        },
                    }))
                );
            } catch (e) {
                console.log(e);
            }
        });
    },
});

const getTodos = createAsyncThunk("todos/getTodos", async () => {
    return await Provider.todoApi
        .getTodos({
            customError: "Could not fetch list of todos, please check your api connection",
        })
        .catch(() => []);
});

const addTodo = createAsyncThunk("todos/addTodo", async (todo: Todo) => {
    const createdTodo = await Provider.todoApi
        .addTodo(todo, {
            customError: `Could not save ${todo.title}. Changes will be lost by page reload`,
        })
        .catch(() => todo);
    return createdTodo;
});

const deleteToDo = createAsyncThunk("todos/deleteTodo", async (ids: string[]) => {
    await Provider.todoApi
        .removeTodos(ids, {
            customError: `Could not delete selected todo(s). Changes will be lost by page reload`,
        })
        .catch(() => ids);
    return ids;
});

const toggleTodoStatus = createAsyncThunk("todos/updateTodoStatus", async (ids: string[]) => {
    const updatedTodos = await Provider.todoApi
        .toggleTodosStatus(ids, {
            customError: `Could not toggle status of selected todo(s). Changes will be lost by page reload`,
        })
        .catch(() => Promise.reject(ids));
    return updatedTodos;
});

export { getTodos, addTodo, deleteToDo, toggleTodoStatus };
export const { titleFilterChanged } = todosSlice.actions;
const todosReducer = todosSlice.reducer;
export { todosReducer };

export const {
    selectAll: selectAllTodos,
    selectById: selectTodoById,
    selectIds: selectTodoIds,
} = todosAdapter.getSelectors((state: AppState) => state.todos);

export const selectFilteredTodoIds = createSelector(
    [selectAllTodos, state => state.todos.filter.title],
    (todos, titleFilter) =>
        todos
            .filter(
                todo =>
                    titleFilter === "" ||
                    todo.title.toLowerCase().includes(titleFilter.toLowerCase())
            )
            .map(todo => todo.id)
);
