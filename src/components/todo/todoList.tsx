import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableHead from "@mui/material/TableHead";
import TableContainer from "@mui/material/TableContainer";
import TableRow from "@mui/material/TableRow";
import LinearProgress from "@mui/material/LinearProgress";
import Paper from "@mui/material/Paper";
import Checkbox from "@mui/material/Checkbox";
import TodoListHead from "./todoListHead";
import { getTodos, selectFilteredTodoIds } from "reducer";
import { AppDispatch, AppState } from "store";
import ToDoListItem from "./todoListItem";
import TodoListToolbar from "./todoListToolbar";

const ToDoList: React.FC = () => {
    const dispatch = useDispatch<AppDispatch>();
    const todoIds = useSelector((state: AppState) => selectFilteredTodoIds(state));
    const loading = useSelector((state: AppState) => state.todos.loading);
    const [selectedIds, setSelectedIds] = useState<string[]>([]);

    useEffect(() => {
        dispatch(getTodos());
    }, [dispatch]);

    const todoIdsString = JSON.stringify(todoIds);
    useEffect(() => {
        setSelectedIds([]);
    }, [todoIdsString]);

    const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.checked) {
            setSelectedIds(todoIds);
            return;
        }
        setSelectedIds([]);
    };

    const handleSelectTodoClick = (event: React.ChangeEvent<HTMLInputElement>, id: string) => {
        if (event.target.checked) {
            setSelectedIds([...selectedIds, id]);
        } else {
            setSelectedIds(selectedIds.filter(selectedId => selectedId !== id));
        }
    };

    return (
        <>
            <TodoListHead />
            <Paper>
                <TodoListToolbar selectedIds={selectedIds} />
                <TableContainer>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell padding="checkbox">
                                    <Checkbox
                                        color="primary"
                                        indeterminate={
                                            selectedIds.length > 0 &&
                                            selectedIds.length < todoIds.length
                                        }
                                        checked={
                                            todoIds.length > 0 &&
                                            selectedIds.length === todoIds.length
                                        }
                                        onChange={handleSelectAllClick}
                                    />
                                </TableCell>
                                <TableCell sx={{ fontWeight: "bold" }}>Title</TableCell>
                                <TableCell sx={{ fontWeight: "bold" }}>Status</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {loading ? (
                                <TableRow key="loading">
                                    <TableCell colSpan={3}>
                                        <LinearProgress />
                                    </TableCell>
                                </TableRow>
                            ) : (
                                todoIds.map(id => (
                                    <ToDoListItem
                                        key={id}
                                        id={id}
                                        onSelectClick={handleSelectTodoClick}
                                        selected={selectedIds.includes(id)}
                                    />
                                ))
                            )}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
        </>
    );
};

export default ToDoList;
