import { useEffect, useState } from "react";
import Provider from "api";

const useTodoTitle = (fetch: any) => {
    const [state, setState] = useState({
        loading: false,
        title: "",
    });

    useEffect(() => {
        (async () => {
            if (fetch.refetch) {
                setState({ ...state, loading: true });
                const title = await Provider.todoApi
                    .getTodoTitle({
                        customError:
                            "Could not generate todo title, please check your api connection",
                    })
                    .catch((error: Error) => console.log(error.message));
                setState({ title, loading: false });
            }
        })();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [fetch]);

    return {
        ...state,
    };
};

export { useTodoTitle };
