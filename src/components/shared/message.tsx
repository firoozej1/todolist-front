import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import { SnackbarKey, useSnackbar } from "notistack";
import { AppState } from "store";

const Message: React.FC = () => {
    const message = useSelector((state: AppState) => state.message);
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    useEffect(() => {
        if (message.text) {
            enqueueSnackbar(message.text, {
                anchorOrigin: {
                    vertical: "top",
                    horizontal: "center",
                },
                persist: true,
                action,
            });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [message]);

    const action = (snackbarId: SnackbarKey) => (
        <IconButton size="small" color="inherit" onClick={() => closeSnackbar(snackbarId)}>
            <CloseIcon fontSize="small" />
        </IconButton>
    );

    return null;
};

export default Message;
