enum TodoStatus {
    DONE = 1,
    UNDONE = 0,
}
export { TodoStatus };
