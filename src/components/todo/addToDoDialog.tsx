import React, { useEffect, useState } from "react";
import Input from "@mui/material/Input";
import InputLabel from "@mui/material/InputLabel";
import FormControl from "@mui/material/FormControl";
import CircularProgress from "@mui/material/CircularProgress";
import { useDispatch, useSelector } from "react-redux";
import { v4 as uuid } from "uuid";
import { addTodo } from "reducer";
import { CustomDialog } from "components/shared";
import { TodoStatus } from "types";
import { AppDispatch, AppState } from "store";
import { useTodoTitle } from "./useTodoTitle";

type PropsType = {
    onClose: Function;
    open: boolean;
};
const AddToDoDialog: React.FC<PropsType> = ({ onClose, open }) => {
    const dispatch = useDispatch<AppDispatch>();
    const addLoading = useSelector((state: AppState) => state.todos.addLoading);

    const [title, setTitle] = useState("");
    const [error, setError] = useState(false);
    const [refecth, setRefetch] = useState({ refetch: false });
    const { loading, title: generatedTaskTitle } = useTodoTitle(refecth);

    useEffect(() => {
        if (generatedTaskTitle) {
            setTitle(generatedTaskTitle);
        }
    }, [generatedTaskTitle]);

    useEffect(() => {
        if (open) {
            setRefetch({ refetch: true });
            setError(false);
        }
    }, [open]);

    useEffect(() => {
        if (!addLoading) {
            setTitle("");
            onClose();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [addLoading]);

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setTitle(event.target.value);
        setError(event.target.value === "");
    };

    const handleAddToDo = () => {
        if (title !== "") {
            dispatch(addTodo({ id: uuid(), title, status: TodoStatus.UNDONE }));
        } else {
            setError(true);
        }
    };

    return (
        <CustomDialog
            title="Add ToDo"
            open={open}
            onClose={onClose}
            onOk={handleAddToDo}
            confirmLoading={addLoading}>
            <FormControl variant="standard" error={error} fullWidth>
                <InputLabel>Title</InputLabel>
                <Input
                    value={title}
                    onChange={handleChange}
                    disabled={loading}
                    startAdornment={loading ? <CircularProgress size="1rem" /> : null}
                />
            </FormControl>
        </CustomDialog>
    );
};

export default AddToDoDialog;
