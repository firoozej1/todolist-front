import React from "react";
import Toolbar from "@mui/material/Toolbar";
import { CustomButton } from "components/shared";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, AppState } from "store";
import { deleteToDo, toggleTodoStatus } from "reducer";

type PropsType = {
    selectedIds: string[];
};

const TodoListToolbar: React.FC<PropsType> = ({ selectedIds }) => {
    const dispatch = useDispatch<AppDispatch>();
    const changeStatusLoading = useSelector((state: AppState) => state.todos.changeStatusLoading);
    const deleteLoading = useSelector((state: AppState) => state.todos.deleteLoading);

    const handleChangeStatus = () => {
        dispatch(toggleTodoStatus(selectedIds));
    };

    const handleDelete = () => {
        dispatch(deleteToDo(selectedIds));
    };
    return (
        <Toolbar>
            <CustomButton
                disabled={selectedIds.length === 0 || changeStatusLoading}
                onClick={handleChangeStatus}
                sx={{ marginRight: "10px" }}
                loading={changeStatusLoading}>
                Toggle Todo Status
            </CustomButton>
            <CustomButton
                color="error"
                disabled={selectedIds.length === 0 || deleteLoading}
                onClick={handleDelete}
                loading={deleteLoading}>
                Delete Todo
            </CustomButton>
        </Toolbar>
    );
};

export default TodoListToolbar;
